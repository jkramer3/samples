/**
BSD 3-Clause License

Copyright (c) 2021, James Kramer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
…AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPO…LL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.jkramer3.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/** Utility class for handling JSON manipulation. Some of the methods
 * in this class just make other code less verbose (i.e., handle the
 * necessary try/catch of <code>JSONException</code>) or better handle
 * String values and JSON (better handling of escape characters). */
public final class JsonUtil {

	private static Logger logger = Logger.getLogger(JsonUtil.class);
	 
	/** Front-end for {@link #getJsonPathMap(JSONObject) getJsonPathMap}.
	 * @param jsonStr A JSON object in <code>String</code> form
	 * @return A <code>Map</code> whose keys specify every location
	 *   in the JSON object in dot-notation
	 * @throws IllegalArgumentException If <code>jsonStr</code> is
	 *   not valid JSON */
	public static final Map<String,Object> getJsonPathMap(String jsonStr)
			throws IllegalArgumentException {
		Map<String,Object> pathMap = null;
		try {
			if (JsonUtil.isJsonArray(jsonStr)) {
				JSONArray array = new JSONArray(jsonStr);
				pathMap = getJsonPathMap(array);
			} else {
				JSONObject json = new JSONObject(jsonStr);
				pathMap = getJsonPathMap(json);
			}
		} catch (JSONException je) {
			throw new IllegalArgumentException("Cannot get structure paths", je);
		}
		return pathMap;
	}
	
	/** Utility method to get a <code>Map</code> of key strings (in
	 * dot-notation) and their associated values from a JSON string.
	 * @param json A JSON object
	 * @return A <code>Map</code> whose keys specify every location
	 *   in the JSON object in dot-notation */
	public static final Map<String,Object> getJsonPathMap(JSONObject json) {
		HashMap<String,Object> pathMap = new HashMap<String,Object>();
		getJsonPathMapHelper("", json, pathMap);
		//logger.info("getJsonPathMap: return Map: "+ pathMap);
		return pathMap;
	}
	
	/** Utility method to get a <code>Map</code> of key strings (in
	 * dot-notation) and their associated values from a JSON string.
	 * @param json A JSON Array
	 * @return A <code>Map</code> whose keys specify every location
	 *   in the JSON array by index */
	public static final Map<String, Object> getJsonPathMap(JSONArray json) {
		Map<String,Object> pathMap = null;
		try {
			JSONObject jobj = new JSONObject();
			for (int i = 0; i < json.length(); i++) {
				jsonPut(jobj, String.valueOf(i), json.get(i));
			}
			pathMap = getJsonPathMap(jobj);
		} catch (JSONException je) {
			logger.error("JSON index failed: " + je.getLocalizedMessage());
		}
		return pathMap;
	}

	/** Recursive helper method for {@link #getJsonPathMap(JSONObject)
	 * getJsonObject}.
	 * @param path The dot-notation path already processed
	 * @param json The unprocessed JSON (sub-)object
	 * @param pathMap The <code>Map</code> (containing already processed
	 *   key/value pairs) in which to store key/value pairs */
	private static void getJsonPathMapHelper(String path, JSONObject json, HashMap<String,Object> pathMap) {
		String prefix = (path.trim().isEmpty() ? "" : path +".");
		Iterator<?> it = json.keys();
		while (it.hasNext()) {
			String key = (String)it.next();
			Object obj = null;
			try {
				obj = json.get(key);
			} catch (JSONException je) {
				// this cannot happen, 'cuz key comes from json object!
				// famous last words...so at least log an error
				logger.error("JSON key failed: "+ je.getLocalizedMessage());
			}
			if (obj == null) {
				// WTH? JSON failure (see above), already output message
			} else if (obj instanceof JSONObject) {
				getJsonPathMapHelper(path+key, (JSONObject)obj, pathMap);
			} else {
				// key's value is not a JSONObject, so no subpath
				pathMap.put(prefix + key, obj);
			}
		}
	}
	
	/** Utility method to obtain the common structure between two
	 * JSON objects.
	 * @param json1 A JSON object
	 * @param json2 A JSON object
	 * @return The <code>Set</code> of paths, in dot-notation, that
	 *   the two JSON object have in common */
	public final static Set<String> jsonPathIntersect(JSONObject json1, JSONObject json2)
			throws IllegalArgumentException {
		if (json1 == null || json2 == null) {
			throw new IllegalArgumentException("Cannot intersect null JSON!");
		}
		Map<String,Object> map1 = getJsonPathMap(json1);
		Map<String,Object> map2 = getJsonPathMap(json2);
		boolean map1Smaller = (map1.size() > map2.size() ? false : true);
		
		HashSet<String> common = new HashSet<String>();
		for (String key : (map1Smaller ? map1.keySet() : map2.keySet())) {
			if (map1Smaller && map2.containsKey(key)) {
				common.add(key);
			} else if (!map1Smaller && map1.containsKey(key)) {
				common.add(key);
			}
		}
		return common;
	}
	
	/** Front-end for {@link jsonIntersectEquals(JSONObject,JSONObject,boolean)
	 * jsonIntersectEquals} that does NOT output differences.
	 * 
	 * @param json1 A JSON object
	 * @param json2 A JSON object
	 * @return Whether the values associated with the common paths are
	 *   equal */
	public final static boolean jsonIntersectEquals(JSONObject json1, JSONObject json2)
			throws IllegalArgumentException {
		return jsonIntersectEquals(json1, json2, false);
	}

	/** Utility method to do a deep equality check of the common structure
	 * between two JSON objects.
	 * @param json1 A JSON object
	 * @param json2 A JSON object
	 * @param spew Output unequal values
	 * @return Whether the values associated with the common paths are
	 *   equal */
	public final static boolean jsonIntersectEquals(JSONObject json1, JSONObject json2, boolean spew)
			throws IllegalArgumentException {
		if (json1 == null || json2 == null) {
			throw new IllegalArgumentException("Cannot intersect null JSON!");
		}
		Map<String,Object> map1 = getJsonPathMap(json1);
		Map<String,Object> map2 = getJsonPathMap(json2);
		Set<String> common = jsonPathIntersect(json1, json2);
		StringBuilder sb = new StringBuilder();
		for (String key : common) {
			Object obj1 = map1.get(key), obj2 = map2.get(key);
			boolean equals = false;
			if (obj1 instanceof JSONObject && obj2 instanceof JSONObject) {
				// TODO: make this recursive 'jsonIntersectEquals'?
				equals = jsonEquals((JSONObject)obj1, (JSONObject)obj2);
			} else if (obj1 instanceof JSONArray && obj2 instanceof JSONArray) {
				equals = jsonEquals((JSONArray)obj1, (JSONArray)obj2);
			} else {
				equals = obj1.equals(obj2);
			}
			if (!equals) {
				sb.append("  key [");
				sb.append(key);
				sb.append("]: ");
				sb.append(obj1.toString());
				sb.append(" != ");
				sb.append(obj2.toString());
				sb.append(System.lineSeparator());
			}
		}
		boolean same = sb.length() < 1;
		if (same) {
			if (spew) {
				logger.info("No differences: equal");
			}
		} else if (spew) {
			sb.insert(0, "Differences found:\n");
			logger.info(sb.toString());
		}
		return same;
	}
	
	/** Utility method to do a cursory equality check (string conversion,
	 * then equality) between two JSON objects.
	 * @param json1 A JSON object
	 * @param json2 A JSON object
	 * @return Whether the JSON strings are equal */
	public final static boolean jsonEquals(JSONObject json1, JSONObject json2)
			throws IllegalArgumentException {
		if (json1 == null && json2 == null) {
			return true;
		} else if (json1 == null || json2 == null) {
			throw new IllegalArgumentException("Cannot compare null JSON!");
		}
		JSONObject compare = new JSONObject();
		jsonPut(compare, "json1", json1.toString());
		jsonPut(compare, "json2", json2.toString());
		return getJsonValue(compare, "json1").equals(getJsonValue(compare, "json2"));
	}
	
	/** Utility method to do a deep equality check between two JSON objects,
	 * converting values to Strings for comparison.
	 * 
	 * @param json1 A JSON object
	 * @param json2 A JSON object
	 * @return Whether the JSON objects are equal */
	public final static boolean jsonDeepEquals(JSONObject json1, JSONObject json2)
			throws IllegalArgumentException {
		return jsonDeepEquals(json1, json2, false);
	}
	
	/** Utility method to do a deep equality check between two JSON objects,
	 * converting values to Strings for comparison.
	 * 
	 * @param json1 A JSON object
	 * @param json2 A JSON object
	 * @param spew Output unequal key/values
	 * @return Whether the JSON objects are equal */
	public final static boolean jsonDeepEquals(JSONObject json1, JSONObject json2, boolean spew)
			throws IllegalArgumentException {
		if (json1 == null && json2 == null) {
			return true;
		} else if (json1 == null || json2 == null) {
			throw new IllegalArgumentException("Cannot compare null JSON!");
		}
		Map<String,Object> map1 = getJsonPathMap(json1);
		Map<String,Object> map2 = getJsonPathMap(json2);
		if (map1.size() != map2.size()) {
			throw new IllegalArgumentException("Different JSON lengths!");
		}
		StringBuilder sb = new StringBuilder();
		for (String key : map1.keySet()) {
			boolean equals = false;
			Object obj1 = map1.get(key), obj2 = "null";
			if (map2.containsKey(key)) {
				obj2 = map2.get(key);
				if (obj1 instanceof String && !(obj2 instanceof String)) {
					// stupid JSON chops decimals, which is infuriating
					// note also that real str obj1 equals int obj2, 'cuz Number
					if (obj2 instanceof Number) {
						equals = Double.parseDouble((String)obj1) == ((Number)obj2).doubleValue();
					} else {
						equals = obj1.equals(obj2.toString());
					}
				} else if (!(obj1 instanceof String) && obj2 instanceof String) {
					// stupid JSON chops decimals, which is infuriating
					// note also that int obj1 equals real str obj2, 'cuz Number
					if (obj1 instanceof Number) {
						equals = Double.parseDouble((String)obj2) == ((Number)obj1).doubleValue();
					} else {
						equals = obj2.equals(obj1.toString());
					}
				} else if (obj1 instanceof JSONObject && obj2 instanceof JSONObject) {
					equals = jsonDeepEquals((JSONObject)obj1, (JSONObject)obj2);
				} else if (obj1 instanceof JSONArray && obj2 instanceof JSONArray) {
					equals = jsonEquals((JSONArray)obj1, (JSONArray)obj2);
				} else {
					equals = obj1.equals(obj2);
				}
			}
			if (!equals) {
				sb.append("  key [");
				sb.append(key);
				sb.append("]: ");
				sb.append(obj1.toString());
				sb.append(" != ");
				sb.append(obj2.toString());
				sb.append(System.lineSeparator());
			}
		}
		boolean same = sb.length() < 1;
		if (same) {
			if (spew) {
				logger.info("No differences: equal");
			}
		} else if (spew) {
			sb.insert(0, "Differences found:\n");
			logger.info(sb.toString());
		}
		return same;
	}
	
	/** Utility method to do a cursory equality check (string conversion,
	 * then equality) between two JSON arrays.
	 * @param json1 A JSON object
	 * @param json2 A JSON object
	 * @return Whether the JSON strings are equal */
	public final static boolean jsonEquals(JSONArray json1, JSONArray json2)
			throws IllegalArgumentException {
		if (json1 == null && json2 == null) {
			return true;
		} else if (json1 == null || json2 == null) {
			throw new IllegalArgumentException("Cannot compare null JSON!");
		}
		JSONObject compare = new JSONObject();
		jsonPut(compare, "json1", json1.toString());
		jsonPut(compare, "json2", json2.toString());
		return getJsonValue(compare, "json1").equals(getJsonValue(compare, "json2"));
	}
	
	/** Utility method to clear an existing <code>JSONObject</code>. */
	public final static boolean jsonClear(JSONObject json) {
		boolean retval = false;
		if (json == null) {
			logger.error("Cannot clear null JSON!");
		} else if (json.length() == 0) {
			retval = true;
		} else {
			Iterator<?> it = json.keys();
			while (it.hasNext()) {
				String key = (String)it.next();
				json.remove(key);
			}
			retval = (json.length() == 0);
		}
		return retval;
	}
	
	/** Check whether the given <code>String</code> is a <code>JSONArray</code>.
	 * This method is NOT efficient, as the check consists of actually
	 * parsing the string into JSON. */
	public final static boolean isJsonArray(String val) {
		boolean retval = false;
		if (val != null) {
			// TODO: originally, this checked for a JSONObject, THEN for
			// a JSONArray; want this functionality, but not here!
			//// rather than nested try/catch blocks, used 'added'
			try {
				@SuppressWarnings("unused")
				JSONArray jsonCheck = new JSONArray(val);
				retval = true;
			} catch (JSONException je) {
				// neither array nor object, so false
			}
			 
		}
		return retval;
	}
	
	/** Utility method to copy a JSONObject. Note that both a
	 * <code>null</code> parameter and a copy error will return
	 * <code>null</code>. */
	public final static JSONObject jsonCopy(JSONObject json) {
		if (json == null) {
			return null;
		}
		try {
			return new JSONObject(json.toString());
		} catch (JSONException je) {
			return null;
		}
	}
	
	/** Utility method to copy a JSONArray. Note that both a
	 * <code>null</code> parameter and a copy error will return
	 * <code>null</code>. */
	public final static JSONArray jsonCopy(JSONArray json) {
		if (json == null) {
			return null;
		}
		try {
			return new JSONArray(json.toString());
		} catch (JSONException je) {
			return null;
		}
	}
	
	/** Utility method to copy an array into a JSONArray. Note that both
	 * a <code>null</code> parameter and a copy error will return
	 * <code>null</code>. */
	public final static JSONArray jsonCopy(Object[] arr) {
		if (arr == null) {
			return null;
		}
		try {
			JSONArray json = new JSONArray();
			for (int i=0; i<arr.length; i++) {
				json.put(i, arr[i]);
			}
			return json;
		} catch (JSONException je) {
			return null;
		}
	}
	
	/** Utility method to copy a List into a JSONArray. Note that both
	 * a <code>null</code> parameter and a copy error will return
	 * <code>null</code>. */
	public final static JSONArray jsonCopy(List<?> list) {
		if (list == null) {
			return null;
		}
		try {
			JSONArray json = new JSONArray();
			for (int i=0; i<list.size(); i++) {
				if (list.get(i) instanceof Double) {
					json.put(i, (!Double.isFinite((Double)list.get(i)) ? "NaN" : list.get(i)));
				} else {
					json.put(i, list.get(i));
				}
			}
			return json;
		} catch (JSONException je) {
			return null;
		}
	}
	
	/** Utility method for putting a key/value pair into a JSONObject.
	 * If the <code>Object</code> is a String representation of JSON,
	 * the string will be converted to JSON prior to the put, so as to
	 * avoid string escape characters upon subsequent conversion.
	 * Increases code readability by relocating what might otherwise be
	 * a series of try/catch blocks. */
	public final static void jsonPut(JSONObject json, String key, Object val) {
		if (json == null) {
			logger.error("Cannot put "+ key +" in null JSON!");
			return;
		} else if (key == null || key.trim().isEmpty()) {
			logger.error("Cannot put empty key in JSON!");
			return;
		} else if (val == null) {
			logger.warn("Disallowing null for [" + key +"] in JSON!");
			return;
		}
		if (val instanceof String) {
			jsonPut(json, key, (String)val);
		} else {
			try {
				json.put(key, val);
			} catch (JSONException je) {
				logger.error("Putting "+ key +": "+ je.getLocalizedMessage());
			}
		}
	}
	
	/** Utility method to put a <code>String</code> into a
	 * <code>JSONObject</code> (avoiding escape characters). */
	public final static void jsonPut(JSONObject json, String key, String val) {
		if (json == null) {
			logger.error("Cannot put "+ key +" in null JSON object!");
			return;
		} else if (key == null || key.trim().isEmpty()) {
			logger.error("Cannot put empty key in JSON!");
			return;
		} else if (val == null) {
			logger.warn("Disallowing null for [" + key +"] in JSON!");
			return;
		}
		// rather than nested try/catch blocks, used 'added'
		boolean added = false;
		try {
			JSONObject jsonCheck = new JSONObject(val);
			json.put(key, jsonCheck);
			added = true;
		} catch (JSONException je) {
			// not an object, try array
		}
		if (!added) {
			try {
				JSONArray jsonCheck = new JSONArray(val);
				json.put(key, jsonCheck);
				added = true;
			} catch (JSONException je) {
				// not an array, default to the string
			}
		}
		if (!added) {
			try {
				json.put(key, val);
			} catch (JSONException je) {
				logger.error("Putting non-json "+ key +": "+ je.getLocalizedMessage());
			}
		}
	}
	
	/** Add a boolean value to a <code>JSONArray</code>. */
	public final static void jsonPut(JSONArray json, Boolean b) {
		json.put(b.booleanValue());
	}
	
	/** Add an integer value to a <code>JSONArray</code>. */
	public final static void jsonPut(JSONArray json, Integer i) {
		json.put(i.intValue());
	}
	
	/** Add a double value to a <code>JSONArray</code>, substituting the
	 * String <code>NaN</code> if the value is not finite. */
	public final static void jsonPut(JSONArray json, Double d) {
		json.put(!Double.isFinite(d) ? "NaN" : d.doubleValue());
	}
	
	/** Add a long value to a <code>JSONArray</code>. */
	public final static void jsonPut(JSONArray json, Long l) {
		json.put(l.longValue());
	}
	
	/** Front-end utility method for {@link #jsonPut(JSONArray,String)
	 * jsonPut} that passes <code>obj.toString()</code> as a parameter. */
	public final static void jsonPut(JSONArray json, Object obj) {
		if (obj == null) {
			logger.error("Cannot put null object in JSON!");
			return;
		}
		jsonPut(json, obj.toString());
	}
	
	/** Utility method to put a <code>String</code> into a
	 * <code>JSONArray</code> (avoiding escape characters). */
	public final static void jsonPut(JSONArray json, String val) {
		if (json == null) {
			logger.error("Cannot put value in null JSON array!");
			return;
		}
		// rather than nested try/catch blocks, used 'added'
		boolean added = false;
		try {
			JSONObject jsonCheck = new JSONObject(val);
			json.put(jsonCheck);
			added = true;
		} catch (JSONException je) {
			// not an object, try array
		}
		if (!added) {
			try {
				JSONArray jsonCheck = new JSONArray(val);
				json.put(jsonCheck);
				added = true;
			} catch (JSONException je) {
				// not an array, default to the string
			}
		}
		if (!added) {
			json.put(val);
		}
	}
	
	/** Utility method to get a String value from a JSONObject with the
	 * specified key. Returns null if <i>json</i> is null or does not
	 * have the specified key; otherwise returns the value or '?' if
	 * a JSONException occurs. */
	public final static String getJsonValue(JSONObject json, String key) {
		String val = null;
		if (json != null && json.has(key)) {
			// values for all the keys we use should be Strings
			try {
				val = json.getString(key);
			} catch (JSONException je) {
				val = "?";
			}
		}
		return val;
	}
	
	/** Utility method to get a boolean value from a JSONObject with the
	 * specified key. Returns false on any exception encountered. */
	public final static boolean getJsonBool(JSONObject json, String key) {
		boolean val = false;
		if (json != null && json.has(key)) {
			try {
				val = json.getBoolean(key);
			} catch (JSONException je) {
				logger.error("getJsonBool: "+ je.getLocalizedMessage());
			}
		}
		return val;
	}
	
	/** Utility method to get a boolean value from a JSONArray at the
	 * specified index. Returns false on any exception encountered. */
	public final static boolean getJsonBool(JSONArray json, int ix) {
		boolean val = false;
		if (json != null && 0 <= ix && ix < json.length()) {
			try {
				val = json.getBoolean(ix);
			} catch (JSONException je) {
				logger.error("getJsonBool: "+ je.getLocalizedMessage());
			}
		}
		return val;
	}
	
	/** Utility method to get an integer value from a JSONObject with the
	 * specified key. Because java doesn't have a notion of an integer
	 * type that isn't a number, there is no acceptable default value, so
	 * throw the exception on any issues.
	 * @throws IllegalArgumentException On any exception encountered. */
	public final static int getJsonInt(JSONObject json, String key)
			throws IllegalArgumentException {
		int val = 0;
		if (json != null && json.has(key)) {
			try {
				val = json.getInt(key);

			} catch (JSONException e) {
				logger.error("getJsonInt: "+ e.toString());
				throw new IllegalArgumentException(e);
			}
		}
		return val;
	}
	
	/** Utility method to get an integer value from a JSONArray at the
	 * specified index. Because java doesn't have a notion of an integer
	 * type that isn't a number, there is no acceptable default value, so
	 * throw the exception on any issues.
	 * @throws IllegalArgumentException On any exception encountered. */
	public final static int getJsonInt(JSONArray json, int ix)
			throws IllegalArgumentException {
		int val = 0;
		if (json != null && 0 <= ix && ix < json.length()) {
			try {
				val = json.getInt(ix);
			} catch (JSONException e) {
				logger.error("getJsonInt: "+ e.toString());
				throw new IllegalArgumentException(e);
			}
		}
		return val;
	}
	
	/** Utility method to get a long value from a JSONObject with the
	 * specified key. Because java doesn't have a notion of an integer
	 * type that isn't a number, there is no acceptable default value, so
	 * throw the exception on any issues.
	 * @throws IllegalArgumentException On any exception encountered. */
	public final static long getJsonLong(JSONObject json, String key) {
		long val = 0;
		if (json != null && json.has(key)) {
			try {
				val = json.getLong(key);

			} catch (JSONException e) {
				logger.error("getJsonLong: "+ e.toString());

				throw new IllegalArgumentException(e);
			}
		}
		return val;
	}
	
	/** Utility method to get a double value from a JSONObject with the
	 * specified key. Returns {@link Double#NaN NaN} for anything other
	 * than a finite value for the specified key (including any exception
	 * encountered). */
	public final static double getJsonDouble(JSONObject json, String key) {
		double val = Double.NaN;
		if (json != null && json.has(key)) {
			try {
				val = json.getDouble(key);
			} catch (JSONException je) {
				// not a number (so not finite), try a parseable string
				val = prideDouble(getJsonString(json, key));
			}
		}
		return val;
	}
	
	/** Utility method to get a double value from a JSONArray at the
	 * specified index. Returns {@link Double#NaN NaN} for anything other
	 * than a finite value for the specified index (including any exception
	 * encountered). */
	public final static double getJsonDouble(JSONArray json, int ix) {
		double val = Double.NaN;
		if (json != null && 0 <= ix && ix < json.length()) {
			try {
				val = json.getDouble(ix);
			} catch (JSONException je) {
				// not a number (so not finite), try a parseable string
				val = prideDouble(getJsonString(json, ix));
			}
		}
		return val;
	}
	
	/** Utility method to translate a String into a double, such that any
	 * non-finite value is interpreted as {@link Double#NaN NaN}. Although
	 * this method is not JSON specific, it is located in this utility
	 * class because it is only needed/used by the {@link #getJsonDouble
	 * getJsonDouble} method and it needed to go somewhere. */
	public static final double prideDouble(String doubleStr) {
		double val = Double.NaN;
		try {
			val = Double.parseDouble(doubleStr);
			if (!Double.isFinite(val)) {
				// for PRIDE, make all non-finite doubles NaNs
				val = Double.NaN;
			}
		} catch (Exception e) {
			// any exception remains NaN
		}
		return val;
	}
	
	/** Utility method to get an Object value from a JSONObject with the
	 * specified key. Returns <code>null</code> on any exception encountered. */
	public final static Object getObject(JSONObject json, String key) {
		Object val = null;
		if (json != null && json.has(key)) {
			try {
				val = json.get(key);
			} catch (JSONException je) {
				logger.error("getObject: "+ je.getLocalizedMessage());
			}
		}
		return val;
	}

	
	/** Utility method to get an Object value from a JSONArray at the
	 * specified index. Returns <code>null</code> on any exception
	 * encountered. */
	public final static Object getObject(JSONArray json, int index) {
		Object val = null;
		if (json != null && json.length() > index) {
			try {
				val = json.get(index);
			} catch (JSONException je) {
				logger.error("getObject: "+ je.getLocalizedMessage());
			}
		}
		return val;
	}
	
	/** Utility method to get a JSONObject value from a JSONObject with the
	 * specified key. Returns <code>null</code> on any exception encountered. */
	public final static JSONObject getJsonObject(JSONObject json, String key) {
		JSONObject val = null;
		if (json != null && json.has(key)) {
			try {
				val = json.getJSONObject(key);
			} catch (JSONException je) {
				logger.error("getJsonObject: "+ je.getLocalizedMessage());
			}
		}
		return val;
	}
	
	/** Utility method to get a JSONObject value from a JSONArray at the
	 * specified index. Returns <code>null</code> on any exception
	 * encountered. */
	public final static JSONObject getJsonObject(JSONArray json, int index) {
		JSONObject val = null;
		if (json != null && json.length() > index) {
			try {
				val = json.getJSONObject(index);
			} catch (JSONException je) {
				logger.error("getJsonObject: "+ je.getLocalizedMessage());
			}
		}
		return val;
	}
	
	/** Utility method to get a String value from a JSONObject with the
	 * specified key. Returns <code>null</code> on any exception encountered. */
	public final static String getJsonString(JSONObject json, String key) {
		String val = null;
		if (json != null && json.has(key)) {
			try {
				val = json.getString(key);
			} catch (JSONException je) {
				logger.error("getJsonString: "+ je.getLocalizedMessage());
			}
		}
		return val;
	}
	
	/** Utility method to get a String value from a JSONArray at the
	 * specified index. Returns <code>null</code> on any exception
	 * encountered. */
	public final static String getJsonString(JSONArray json, int ix) {
		String val = null;
		if (json != null && 0 <= ix && ix < json.length()) {
			try {
				val = json.getString(ix);
			} catch (JSONException je) {
				logger.error("getJsonString: "+ je.getLocalizedMessage());
			}
		}
		return val;
	}
	
	public final static JSONArray getJsonArray(Object jObj) {
		JSONArray arr = null;
		if (jObj instanceof JSONArray) {
			arr = (JSONArray)jObj;
		} else if (jObj instanceof String && isJsonArray((String)jObj)) {
			try {
				arr = new JSONArray((String)jObj);
			} catch (JSONException je) {
				logger.error("getJsonArray: "+ je.getLocalizedMessage());
			}
		}
		return arr;
	}
	
	/** Utility method to get a JSONArray value from a JSONObject (as an
	 * Object) with the specified key. Returns null on any exception
	 * encountered. */
	public final static JSONArray getJsonArray(Object jObj, String key) {
		try {
			JSONObject json = (JSONObject)jObj;
			return getJsonArray(json, key);
		} catch (Exception e) {
			// purposely silent
		}
		return null;
	}
	
	/** Utility method to get a JSONArray value from a JSONObject with the
	 * specified key. Returns null on any exception encountered. */
	public final static JSONArray getJsonArray(JSONObject json, String key) {
		JSONArray val = null;
		if (json != null && json.has(key)) {
			try {
				val = json.getJSONArray(key);
			} catch (JSONException je) {
				val = null;
			}
		}
		return val;
	}

	
	/** Utility method to get a JSONArray value from a JSONArray at the
	 * specified index. Returns null on any exception encountered. */
	public final static JSONArray getJsonArray(JSONArray json, int ix) {
		JSONArray val = null;
		if (json != null && 0 <= ix && ix < json.length()) {
			try {
				val = json.getJSONArray(ix);
			} catch (JSONException je) {
				val = null;
			}
		}
		return val;
	}
	
	/** Return the given JSON object as a string in pretty-printed
	 * format (with 2-space indentions IF conversion is valid,
	 * otherwise as a single-line string. */
	public final static String dbgStr(JSONObject json) {
		String retval = null;
		if (json == null) {
			retval = "null";
		} else {
			try {
				retval = json.toString(2);
			} catch (JSONException je) {
				retval = json.toString();
			}
		}
		return retval;
	}
	
	/** Return the given JSON object as a string in pretty-printed
	 * format (with 2-space indentions IF conversion is valid,
	 * otherwise as a single-line string. */
	public final static String dbgStr(JSONArray json) {
		String retval = null;
		if (json == null) {
			retval = "null";
		} else {
			try {
				retval = json.toString(2);
			} catch (JSONException je) {
				retval = json.toString();
			}
		}
		return retval;
	}
	
	/** Convert the given String to a JSON object, then return a String
	 * in pretty-printed format (with 2-space indentions IF conversion is
	 * valid, otherwise as a single-line string. */
	public final static String dbgStr(String jsonStr) {
		String retval = null;
		if (jsonStr == null) {
			retval = "null";
		} else {
			try {
				JSONObject json = new JSONObject(jsonStr);
				retval = dbgStr(json);
			} catch (JSONException je) {
				try {
					JSONArray json = new JSONArray(jsonStr);
					retval = dbgStr(json);
				} catch (JSONException je2) {
					logger.warn("dbgStr: return empty string for invalid JSON: "+ je2.getLocalizedMessage());
					retval = "";
				}
			}
		}
		return retval;
	}
	
}
