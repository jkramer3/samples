/**
 * Put copyright notice here
 */
package com.jkramer3.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import com.jkramer3.util.JsonUtil;

/** Test selected methods from the {@link JsonUtil JsonUtil} class. */
@SuppressWarnings("unused")
public class JsonUtilTest {
	
	@Test
	public void testJsonClear_null() throws Exception {
		assertFalse("Mishandled clearing null JSON",
		             JsonUtil.jsonClear(null));
	}
	
	@Test
	public void testJsonClear_empty() throws Exception {
		JSONObject json = new JSONObject();
		assertTrue("Mishandled clearing empty JSON",
		             JsonUtil.jsonClear(json));
	}
	
	@Test
	public void testJsonClear_Jo() throws Exception {
		String key = "key", val = "value";
		JSONObject json = new JSONObject();
		JsonUtil.jsonPut(json, key, val);
		JsonUtil.jsonClear(json);
		assertTrue("Mishandled clearing JSON", json.length() == 0);
	}
	
	@Test
	public void testIsJsonArray_Ja() throws Exception {
		String jaStr = "[]";
		assertTrue("Failed to detect JSONArray", JsonUtil.isJsonArray(jaStr));
	}
	
	@Test
	public void testIsJsonArray_Jo() throws Exception {
		String joStr = "{}";
		assertFalse("Detected JSONObject as JSONArray",
		             JsonUtil.isJsonArray(joStr));
	}
	
	@Test
	public void testIsJsonArray_Str() throws Exception {
		String strStr = "";
		assertFalse("Detected String as JSONArray",
		             JsonUtil.isJsonArray(strStr));
	}
	
	@Test
	public void testJsonPut_JoO_null() throws Exception {
		JSONObject json = new JSONObject();
		String key = "key";
		Object val = null;
		JsonUtil.jsonPut(json, key, val);
		assertFalse("Put null into JSON", json.has(key));
	}

	@Test
	public void testJsonPut_JoO_Str() throws Exception {
		JSONObject json = new JSONObject();
		String key = "key";
		Object val = "value";
		JsonUtil.jsonPut(json, key, val);
		try {
			assertTrue("Failed to put Object into JSON", json.has(key));
			String got = json.getString(key);
			assertTrue("Failed to make Object a String", got.equals(val));
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPutJoO_Int() throws Exception {
		JSONObject json = new JSONObject();
		String key = "key";
		Integer val = Integer.valueOf(0);
		JsonUtil.jsonPut(json, key, val);
		try {
			assertTrue("Failed to put Integer into JSON", json.has(key));
			Object got = json.get(key);
			assertTrue("Failed to get Integer from JSON", got.equals(val));
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPutJoO_Jo() throws Exception {
		JSONObject json = new JSONObject();
		String key = "key";
		JSONObject val = new JSONObject();
		JsonUtil.jsonPut(json, key, val);
		try {
			assertTrue("Failed to put JSONObject into JSON", json.has(key));
			Object got = json.get(key);
			assertTrue("Failed to get JSONObject from JSON", got.equals(val));
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPutJoO_JoStr() throws Exception {
		JSONObject json = new JSONObject();
		String key = "key";
		String val = "{}";
		JsonUtil.jsonPut(json, key, val);
		try {
			assertTrue("Failed to put JSONObject String into JSON", json.has(key));
			Object got = json.get(key);
			assertTrue("Failed to get unescaped String from JSON", val.equals(got.toString()));
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPutJoO_Ja() throws Exception {
		JSONObject json = new JSONObject();
		String key = "key";
		JSONArray val = new JSONArray();
		JsonUtil.jsonPut(json, key, val);
		try {
			assertTrue("Failed to put JSONArray into JSON", json.has(key));
			Object got = json.get(key);
			assertTrue("Failed to get JSONArray from JSON", got.equals(val));
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPutJoO_JaStr() throws Exception {
		JSONObject json = new JSONObject();
		String key = "key";
		String val = "[]";
		JsonUtil.jsonPut(json, key, val);
		try {
			assertTrue("Failed to put JSONArray String into JSON", json.has(key));
			Object got = json.get(key);
			assertTrue("Failed to get unescaped String from JSON", val.equals(got.toString()));
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPut_JaO_null() throws Exception {
		JSONArray json = new JSONArray();
		Object val = null;
		JsonUtil.jsonPut(json, val);
		assertFalse("Put null into JSON", json.length() > 0);
	}

	@Test
	public void testJsonPutJaO_() throws Exception {
		JSONArray json = new JSONArray();
		Object val = "value";
		JsonUtil.jsonPut(json, val);
		try {
			assertTrue("Failed to add Object to JSON", json.length() > 0);
			Object got = json.get(0);
			assertTrue("Failed to make put Object a String", got.equals(val));
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPutJaO_Int() throws Exception {
		JSONArray json = new JSONArray();
		Integer val = Integer.valueOf(0);
		JsonUtil.jsonPut(json, val);
		try {
			assertTrue("Failed to add Integer into JSON", json.length() > 0);
			Object got = json.get(0);
			assertEquals("Failed to get Integer from JSONArray", got, val);
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPutJaO_Jo() throws Exception {
		JSONArray json = new JSONArray();
		JSONObject val = new JSONObject();
		JsonUtil.jsonPut(json, val);
		try {
			assertTrue("Failed to put JSONObject into JSONArray", json.length() > 0);
			JSONObject got = json.getJSONObject(0);
			assertEquals("Failed to get JSONObject from JSONArray", got.toString(), val.toString());
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPutJaO_JoStr() throws Exception {
		JSONArray json = new JSONArray();
		String val = "{}";
		JsonUtil.jsonPut(json, val);
		try {
			assertTrue("Failed to put JSONObject String into JSONArray", json.length() > 0);
			Object got = json.get(0);
			assertTrue("Failed to get unescaped String from JSONArray", val.equals(got.toString()));
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPutJaO_Ja() throws Exception {
		JSONArray json = new JSONArray();
		JSONArray val = new JSONArray();
		JsonUtil.jsonPut(json, val);
		try {
			assertTrue("Failed to put JSONArray into JSONArray", json.length() > 0);
			JSONArray got = json.getJSONArray(0);
			assertEquals("Failed to get JSONArray from JSONArray", got.toString(), val.toString());
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test
	public void testJsonPutJaO_JaStr() throws Exception {
		JSONArray json = new JSONArray();
		String val = "[]";
		JsonUtil.jsonPut(json, val);
		try {
			assertTrue("Failed to put JSONArray String into JSONArray", json.length() > 0);
			Object got = json.get(0);
			assertTrue("Failed to get unescaped String from JSON", val.equals(got.toString()));
		} catch (JSONException je) {
			// TODO: message!
		}
	}

	@Test(expected=IllegalArgumentException.class)
	public void testGetJsonPathMap_invalid() throws Exception {
		Map<String,Object> map = JsonUtil.getJsonPathMap("");
		assertTrue("Got map item from invalid JSON", map == null);
	}

	@Test
	public void testGetJsonPathMap_Str() throws Exception {
		JSONObject json = new JSONObject();
		String key = "key";
		String val = "value";
		JsonUtil.jsonPut(json, key, val);
		Map<String,Object> map = JsonUtil.getJsonPathMap(json.toString());
		assertTrue("Failed to get single map entry from JSON", map.size() == 1);
		assertTrue("Failed to get 'key' map entry from JSON", map.containsKey(key));
		Object got = map.get(key);
		assertTrue("Failed to get 'key' value from JSON", map.get(key).equals(val));
	}

	@Test
	public void testGetJsonPathMap_Jo() throws Exception {
		JSONObject json = new JSONObject();
		String key = "key";
		String val = "value";
		JsonUtil.jsonPut(json, key, val);
		Map<String,Object> map = JsonUtil.getJsonPathMap(json);
		assertTrue("Failed to get single map entry from JSON", map.size() == 1);
		assertTrue("Failed to get 'key' map entry from JSON", map.containsKey(key));
		Object got = map.get(key);
		assertTrue("Failed to get 'key' value from JSON", got.equals(val));
	}

	@Test
	public void testGetJsonPathMap_JoNested() throws Exception {
		JSONObject json = new JSONObject();
		String key = "key";
		String val = "value";
		JsonUtil.jsonPut(json, key, val);    // {key: value}
		JSONObject jsonSub = new JSONObject();
		String sub = "sub";
		JsonUtil.jsonPut(jsonSub, key, val); // {key: value}
		JsonUtil.jsonPut(json, sub, jsonSub);// {key: value, sub: {key: value}}
		Map<String,Object> map = JsonUtil.getJsonPathMap(json.toString());
		assertTrue("Failed to get map entries, size="+ map.size(), map.size() == 2);
		assertTrue("Failed to get 'key' map entry from JSON", map.containsKey(key));
		Object got = map.get(key);
		assertTrue("Failed to get 'key' value from JSON", got.equals(val));
		
		String keySub = sub +"."+ key;
		assertTrue("Failed to get 'keySub' map entry from JSON", map.containsKey(keySub));
		Object gotKeySub = map.get(keySub);
		assertTrue("Failed 'keySub' value"+ gotKeySub.toString(), gotKeySub.toString().equals(val));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testJsonPathIntersect_nullarg2() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "value1";
		JsonUtil.jsonPut(arg1, key1, val1);
		Set<String> intersect = JsonUtil.jsonPathIntersect(arg1, null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testJsonPathIntersect_nullarg1() {
		JSONObject arg2 = new JSONObject();
		String key2 = "key2"; String val2 = "value2";
		JsonUtil.jsonPut(arg2, key2, val2);
		Set<String> intersect = JsonUtil.jsonPathIntersect(null, arg2);
	}

	@Test
	public void testJsonPathIntersect_none() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "value1";
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		String key2 = "key2"; String val2 = "value2";
		JsonUtil.jsonPut(arg2, key2, val2);
		Set<String> set = JsonUtil.jsonPathIntersect(arg1, arg2);
		assertTrue("Failed not-empty intersection", set.isEmpty());
	}

	@Test
	public void testJsonPathIntersect_common() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "value1";
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		String key2 = "key2"; String val2 = "value2";
		JsonUtil.jsonPut(arg2, key2, val2);
		@SuppressWarnings("unused")
		JSONObject arg3 = new JSONObject();
		String key3 = "key3"; String val3 = "value3";
		JsonUtil.jsonPut(arg1, key3, val3);
		JsonUtil.jsonPut(arg2, key3, val3);
		Set<String> set = JsonUtil.jsonPathIntersect(arg1, arg2);
		assertTrue("Failed empty intersection", set.contains(key3));
	}

	@Test
	public void testJsonDeepEquals_bothnull() {
		boolean same = JsonUtil.jsonDeepEquals(null, null);
		assertTrue("Failed nulls equality", same);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testJsonDeepEquals_nullarg1() {
		JSONObject arg2 = new JSONObject();
		@SuppressWarnings("unused")
		boolean same = JsonUtil.jsonDeepEquals(null, arg2);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testJsonDeepEquals_nullarg2() {
		JSONObject arg1 = new JSONObject();
		boolean same = JsonUtil.jsonDeepEquals(arg1, null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testJsonDeepEquals_size1() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "value1";
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testJsonDeepEquals_size2() {
		JSONObject arg1 = new JSONObject();
		JSONObject arg2 = new JSONObject();
		String key2 = "key2"; String val2 = "value2";
		JsonUtil.jsonPut(arg2, key2, val2);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
	}

	@Test
	public void testJsonDeepEquals_Str() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "value1";
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		JsonUtil.jsonPut(arg2, key1, val1);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
		assertTrue("Failed simple string equality", same);
	}

	@Test
	public void testJsonDeepEquals_neStr() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "value1";
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		String key2 = "key2"; String val2 = "value2";
		JsonUtil.jsonPut(arg2, key2, val2);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
		assertFalse("Failed simple string inequality", same);
	}

	@Test
	public void testJsonDeepEquals_StrIntIntEq() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "1";
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		Integer val2 = 1;
		JsonUtil.jsonPut(arg2, key1, val2);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
		assertTrue("Failed string-int & int equality", same);
	}

	@Test
	public void testJsonDeepEquals_StrIntIntNe() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "1";
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		Integer val2 = 2;
		JsonUtil.jsonPut(arg2, key1, val2);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
		assertFalse("Failed string-int & int inequality", same);
	}

	@Test
	public void testJsonDeepEquals_StrRealRealEq() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "1.0";
		JsonUtil.jsonPut(arg1, key1, val1);
		Double val2 = 1.0;
		JSONObject arg2 = new JSONObject();
		JsonUtil.jsonPut(arg2, key1, val2);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
		assertTrue("Failed string-real & real equality", same);
	}

	@Test
	public void testJsonDeepEquals_StrRealRealNe() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "1.0";
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		Double val2 = 2.0;
		JsonUtil.jsonPut(arg2, key1, val2);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
		assertFalse("Failed string-real & real inequality", same);
	}

	@Test
	public void testJsonDeepEquals_StrIntRealNe() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "1";
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		Double val2 = 1.0;
		JsonUtil.jsonPut(arg2, key1, val2);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
		assertTrue("Failed string-int & real cast inequality", same);
	}

	@Test
	public void testJsonDeepEquals_StrRealIntEq() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1"; String val1 = "1.0";
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		Integer val2 = 1;
		JsonUtil.jsonPut(arg2, key1, val2);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
		assertTrue("Failed string-real & int cast equality", same);
	}

	@Test
	public void testJsonDeepEquals_RealIntEq() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1";
		Double val1 = 1.0;
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		Integer val2 = 1;
		JsonUtil.jsonPut(arg2, key1, val2);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
		assertFalse("Failed real & int inequality", same);
	}

	@Test
	public void testJsonDeepEquals_IntRealEq() {
		JSONObject arg1 = new JSONObject();
		String key1 = "key1";
		Integer val1 = 1;
		JsonUtil.jsonPut(arg1, key1, val1);
		JSONObject arg2 = new JSONObject();
		Double val2 = 1.0;
		JsonUtil.jsonPut(arg2, key1, val2);
		boolean same = JsonUtil.jsonDeepEquals(arg1, arg2);
		assertFalse("Failed int & real inequality", same);
	}

	@Test
	public void testJsonCopy_Jo() throws Exception {
		JSONObject json = new JSONObject();
		JsonUtil.jsonPut(json, "boolean", true);
		JsonUtil.jsonPut(json, "int", 1);
		JsonUtil.jsonPut(json, "real", 1.1);
		JsonUtil.jsonPut(json, "string", "string");
		JSONObject subjson = new JSONObject();
		JsonUtil.jsonPut(subjson, "key1", "key1");
		JsonUtil.jsonPut(json, "json", subjson);
		JSONArray subarr = new JSONArray();
		JsonUtil.jsonPut(subarr, "val1");
		JsonUtil.jsonPut(json, "arr", subarr);
		JSONObject copy = JsonUtil.jsonCopy(json);
		assertTrue("JSONObject copy not equal", JsonUtil.jsonEquals(json, copy));
	}
	
	@Test
	public void testJsonCopy_Ja() throws Exception {
		JSONArray json = new JSONArray();
		JsonUtil.jsonPut(json, true);
		JsonUtil.jsonPut(json, 1);
		JsonUtil.jsonPut(json, 1.1);
		JsonUtil.jsonPut(json, "string");
		JSONObject subjson = new JSONObject();
		JsonUtil.jsonPut(subjson, "key1", "key1");
		JsonUtil.jsonPut(json, subjson);
		JSONArray subarr = new JSONArray();
		JsonUtil.jsonPut(subarr, "val1");
		JsonUtil.jsonPut(json, subarr);
		JSONArray copy = JsonUtil.jsonCopy(json);
		assertTrue("JSONArray copy not equal", JsonUtil.jsonEquals(json, copy));
	}
	
	@Test
	public void testJsonCopy_Oa() throws Exception {
		// note: cursory test, just check a string and an Integer
		Object[] oarr = new Object[]{new String("string"), new Integer(1)};
		JSONArray copy = JsonUtil.jsonCopy(oarr);
		assertTrue("Object[0] copy not equal", JsonUtil.getJsonString(copy, 0).equals(oarr[0]));
		assertTrue("Object[1] copy not equal", JsonUtil.getJsonInt(copy, 1) == (Integer)oarr[1]);
	}
	
	@Test
	public void testJsonCopy_List() throws Exception {
		// note: cursory test, just check a string
		ArrayList<String> list = new ArrayList<String>();
		list.add(new String("string"));
		JSONArray copy = JsonUtil.jsonCopy(list);
		assertTrue("List.get(0) copy not equal", JsonUtil.getJsonString(copy, 0).equals(list.get(0)));
	}
	
	// skipping getJson methods!
	// jsonIntersectEquals covered by jsonPathIntersect/jsonDeepEquals
}
