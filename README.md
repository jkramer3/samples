# Code samples

Repository containing code samples, which is an ongoing process. Developed
and tested (most recently) on Ubuntu 18.04. ROS code assumes `kinetic`,
although no issues have been encountered with `melodic`; Java code assumes
Java 8. ROS packages are contained in their own repositories for cloning
directly into a ROS workspace and accessible by following the provided links.

* [Egosphere (ROS)](#egosphere)
* [Gazebo Magic Brake (ROS)](#gazebo-magic-brake)
* [ROS Param Dialog (ROS)](#ros-param-dialog)
* [JSON Utility (Java)](#json-utility)
* [ADE (Agent Development Environment, Java)](#agent-development-environment)

In addition, here are some videos of projects on which I have worked but
cannot, for various reasons, make code public:

* (2020) [PHARAOH Phase II &mdash; ISS caretaking demonstration (simulation)](https://www.youtube.com/watch?v=Xzv02X2X3uE)
* (2017) [Using PRIDE for TRACBot navigation and manipulation (simulation)](https://www.youtube.com/watch?v=npuH51OtAVU)
* (2014) [Boston Dynamics Atlas mimicking human motion using a Kinect](https://www.youtube.com/watch?v=L_JJ3NWT5pI)

## Egosphere
<p><img src="./images/egosphere_rviz_screenshot.png" alt="RViz Egosphere" width="400" border="3"/></p>
A data structure based on a geodesic sphere for spatio-temporal data. This
supplanted `HashMap` as my favorite data structure for a time, until I
discovered the genius of [OctoMaps](https://octomap.github.io). Please
see the dedicated [egosphere repository](https://gitlab.com/jkramer3/egosphere).

## Gazebo Magic Brake
A gazebo plugin that maintains a robot's position when zero velocities
are applied. Please see the dedicated
[magic brake repository](https://gitlab.com/jkramer3/gazebo-magic-brake).

## ROS Param Dialog
<p><img src="./images/qt-rosparam_screenshot.png" alt="Qt ROS Parameter Dialog" width="400" border="3"/></p>
A simple Qt dialog that retrieves the contents of the ROS parameter server
and presents the values in a `QTreeWidget` for possible modification. Please
see the dedicated [rosparam repository](https://gitlab.com/jkramer3/qt-rosparam-dialog).

## JSON Utility
Including the `com.jkramer3.util.JsonUtil.java` class as a view of my
standard code aesthetic, including JUnit tests.

This is a class containing static methods for handling Jettison's JSON data
objects (v1.1 jar file included). This class started out with the `dbgStr`
methods, written to avoid writing a `try/catch` every time I desired pretty
printed console output. It proceeded to grow as needed; my java code read
much cleaner and the `jsonIntersectEquals` proved particularly useful for
our purposes.

Oh, and guarding against JSON's weak handling of `double` values (e.g.,
`getJsonDouble`) saved many heart-and-headaches and MANY debugging hours.
Specifically, this is because not only are `NaN` values illegal in JSON
but required by one of our clients (which really bothered our front-end
code) but, more perniciously, conversion of a zero-valued decimal to JSON
and back loses its `double` identity, truncating the decimal and thus
masquerading as an `int`.

## Agent Development Environment
ADE is a distributed, failure-tolerant, and introspective agent/robotics
framework I developed while in graduate school at Notre Dame. It served
as the fundamental research platform for dozens of peer-reviewed papers
for over 15 years, with the core functionality surviving well beyond the
end of my tenure in 2007.

Visit the [ADE Welcome page](http://ade.sourceforge.net) for a brief
description of the project; a further link on that page will download
a code tarball (for easy access, here's a direct download link:
[SourceForge](https://sourceforge.net/projects/ade/)).
I wrote the bulk of the code in the `ade/ade` package (the core
functionality), along with many items in the `ade/com` package to provide
fundamental capabilities (e.g., laser range finder, festival speech
production, sphinx speech recognizer, prolog interface, etc.).

